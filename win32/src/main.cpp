//
//  main.cpp
//  HelloWorld
//
//  Created by Admin on 30/01/16.
//  Copyright © 2016 MyCompanyName. All rights reserved.
//

#include <iostream>
#include "hello_world_impl.hpp"

int main(int argc, const char * argv[]) {
    helloworld::HelloWorldImpl hw = helloworld::HelloWorldImpl();
    
    std::string myString = hw.get_hello_world();
    
    std::cout << myString << "\n";
    
    return 0;
}
