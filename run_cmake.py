#!/usr/bin/python

import os
from abc import abstractmethod
from subprocess import call, check_call, CalledProcessError
from enum import Enum
import sys, getopt


# region DOC
# 1) Configure a cmake project.
#
# For all platforms:
# cmake -G{GENERATOR_NAME} -B{BUILD_DIR_PATH} -H{SOURCE_DIR_PATH}
# "-G" - specifies the generator name
# "-B" - specifies path to the build folder
# "-H" - specifies path to the source folder
#
# Get system variable with help of
# os.environ.get("ENV_VARIABLE")
#
# Examples:
# 1-1) Visual Studio win32
# cmake -GVisual Studio 14 -Bbuild/win32/x86 -H.
#
# 1-2) Android
# cmake -G"MinGW Makefiles" \
#  -DCMAKE_TOOLCHAIN_FILE="cmake/android.toolchain.cmake" \
#  -DCMAKE_MAKE_PROGRAM="$ANDROID_NDK/prebuilt/windows-x86_64/bin/make.exe" \
#  -DCMAKE_BUILD_TYPE=Debug \
#  -DANDROID_ABI=x86 \
#  -DANDROID_NATIVE_API_LEVEL=19 \
#  -B"build/android" -H.
#
# 1-3) IOS
# cmake -DCMAKE_TOOLCHAIN_FILE=cmake/ios.toolchain.xcode.cmake \
#  -DIOS_PLATFORM=SIMULATOR \
# -GXcode ..
#

# 2) Build a cmake project.
# 2-1) For Android.
#
# cmake --build {BUILD_DIR_PATH}
#
# 2-2) For Visual Studio and Xcode.
#
# Add --target ALL_BUILD --config {BUILD_TYPE}
# for win32 and ios build due to# multiple build configuration.
#
# cmake --build {BUILD_DIR_PATH} --target ALL_BUILD --config {BUILD_TYPE}
# endregion


class Platfrom(Enum):
    android = 1
    ios = 2
    win32 = 3
    none = 4


class CmakeBase(object):
    def __init__(self, build_types):

        if build_types is not None:
            self.build_types = build_types
        else:
            self.build_types = ["Debug", "Release"]

        self.platform = Platfrom.none
        self.cmake_generator = ""
        self.platform_ABIs = []
        self.toolchain_file = ""
        self.platform_name = "unknown"

    def build(self):
        self.build_cmake(self.platform_ABIs,
                         self.build_types,
                         self.platform,
                         self.cmake_generator,
                         self.toolchain_file)

    def build_cmake(self,
                    platform_ABIs,
                    build_types,
                    platform,
                    cmake_generator,
                    toolchain_file=""):

        print "Platform_name = " + self.platform_name

        # Delete unneeded directories.
        dirs_name_delete = ["build/" + self.platform_name,
                            "bin/" + self.platform_name]

        for dir_name in dirs_name_delete:
            print "Delete dir: " + dir_name

            check_call(["rm", "-rf", dir_name])

        for build_type in build_types:
            for platform_ABI in platform_ABIs:

                # Create build directories.
                dirs_name_create = ["build/" + self.platform_name + "/" + platform_ABI]

                dir_name_binary = "bin/" + self.platform_name + "/" + platform_ABI

                print "dir_name_binary: " + dir_name_binary

                # -p, --parents
                # no error if existing,
                # make parent directories as needed
                for dir_name in dirs_name_create:
                    print "Create dir: " + dir_name

                    check_call(["mkdir", "-p", dir_name])

                    # 1) Configure a cmake project.
                    cmake_args = []
                    cmake_args.append("cmake")
                    cmake_args.append(cmake_generator)
                    cmake_args.append("-B" + dir_name)
                    cmake_args.append("-H.")

                    # Don't pass CMAKE_BUILD_TYPE to the cmake
                    # for the Visual Studio or Xcode
                    # due to multiple build configurations.
                    if platform is Platfrom.android:
                        cmake_args.append("-DCMAKE_BUILD_TYPE=" + build_type)

                    # Pass additional win32 flags.
                    # Pass PLATFORM_ABI to the cmake for the win32 platform.
                    if platform is Platfrom.win32:
                        cmake_args.append("-DPLATFORM_ABI=" + platform_ABI)

                    # Pass toolchain file for Android and IOS platforms.
                    if platform is (Platfrom.android or Platfrom.ios):
                        cmake_args.append("-DCMAKE_TOOLCHAIN_FILE=" + toolchain_file)

                    # Pass additional android flags.
                    if platform is Platfrom.android:
                        cmake_args.append("-DCMAKE_MAKE_PROGRAM="
                                          + os.environ.get("ANDROID_NDK")
                                          + "/prebuilt/windows-x86_64/bin/make.exe")

                        cmake_args.append("-DANDROID_ABI=" + platform_ABI)
                        cmake_args.append("-DANDROID_NATIVE_API_LEVEL=19")

                    # Pass additional ios flags.
                    if platform is Platfrom.ios:
                        cmake_args.append("-DIOS_PLATFORM=" + platform_ABI)

                    print "Cmake args:", cmake_args

                    print "Call cmake configure"
                    check_call(cmake_args)

                    # 2) Build a cmake project.
                    print "Call cmake build for a directory:", dir_name

                    if platform is Platfrom.android:
                        # For Android.
                        check_call(["cmake", "--build", dir_name])
                    else:
                        # For Visual Studio and Xcode.
                        check_call(["cmake", "--build", dir_name,
                                    "--target", "ALL_BUILD",
                                    "--config", build_type])


class CmakeAndroid(CmakeBase):
    def __init__(self, build_types=None):
        super(CmakeAndroid, self).__init__(build_types)

        self.platform = Platfrom.android
        self.platform_ABIs = ["x86", "armeabi-v7a"]
        self.cmake_generator = "-GMinGW Makefiles"
        self.toolchain_file = "cmake/android.toolchain.cmake"
        self.platform_name = "android"

    def build(self):
        super(CmakeAndroid, self).build()


class CmakeIOS(CmakeBase):
    def __init__(self, build_types=None):
        super(CmakeIOS, self).__init__(build_types)

        self.platform = Platfrom.ios
        # self.platform_ABIs = ["SIMULATOR", "OS"]
        self.platform_ABIs = ["SIMULATOR"]
        self.cmake_generator = "-GXcode"
        self.toolchain_file = "cmake/ios.toolchain.xcode.cmake"
        self.platform_name = "ios"

    def build(self):
        super(CmakeIOS, self).build()


class CmakeWin32(CmakeBase):
    def __init__(self, build_types=None):
        super(CmakeWin32, self).__init__(build_types)

        self.platform = Platfrom.win32

        self.platform_ABIs = ["x86"]
        self.cmake_generator = "-GVisual Studio 14"
        self.platform_name = "win32"

    def build(self):
        super(CmakeWin32, self).build()


class CmakeWin64(CmakeBase):
    def __init__(self, build_types=None):
        super(CmakeWin64, self).__init__(build_types)

        self.platform = Platfrom.win32

        self.platform_ABIs = ["Win64"]
        self.cmake_generator = "-GVisual Studio 14 Win64"
        self.platform_name = "win32"

    def build(self):
        super(CmakeWin64, self).build()


class CrossPlatformBuilder(object):
    def __init__(self,
                 build_types,
                 is_android_build,
                 is_ios_build,
                 is_win32_build):
        self.build_types = build_types

        self.is_android_build = is_android_build
        self.is_ios_build = is_ios_build
        self.is_win32_build = is_win32_build

        self.builder = None

    def build_all(self):
        if self.is_win32_build:
            self.build_win32()
            # self.build_win64()

        if self.is_android_build:
            self.build_android()

        if self.is_ios_build:
            self.build_ios()

    def build_win32(self):
        self.builder = CmakeWin32(self.build_types)
        self.builder.build()

    def build_android(self):
        self.builder = CmakeAndroid(self.build_types)
        self.builder.build()

    def build_ios(self):
        self.builder = CmakeIOS(self.build_types)
        self.builder.build()


def check_bool(arg):
    return True if arg.lower() == "1" else False


def check_config(arg):
    build_types = []

    if arg.lower() == "0":
        build_types.append("Debug")
    elif arg.lower() == "1":
        build_types.append("Release")
    elif arg.lower() == "2":
        build_types.append("Debug")
        build_types.append("Release")
    return build_types


def main(argv):
    build_types = []
    is_android_build = True
    is_ios_build = True
    is_win32_build = True

    help = "python run_cmake.py  --config=<0, 1, 2> --android=<0, 1> --ios=<0, 1> --win32=<0, 1>"

    additional_help = "python run_cmake.py  -c<0, 1, 2> -a<0, 1> -i<0, 1> -w<0, 1>\n" \
                      "--config=0 - debug; 1 - release; 2 - debug and release\n" \
                      "--android, --ios, --win32=0 - false; 1 - true"

    default = "--config=0 --android=1 --ios=1 --win32=1\n" \
              "-c0 -a1 -i1 -w1"

    try:
        opts, args = getopt.getopt(argv, "hc:a:i:w:", ["android=",
                                                       "ios=",
                                                       "win32=",
                                                       "config="])
    except getopt.GetoptError:
        print help
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print "help:", help
            print "or:", additional_help
            print
            print "default:", default
            sys.exit()
        elif opt in ("-c", "--config"):
            build_types = check_config(arg)
        elif opt in ("-a", "--android"):
            is_android_build = check_bool(arg)
        elif opt in ("-i", "--ios"):
            is_ios_build = check_bool(arg)
        elif opt in ("-w", "--win32"):
            is_win32_build = check_bool(arg)

    print "build_types =", build_types
    print "is_android_build =", is_android_build
    print "is_ios_build =", is_ios_build
    print "is_win32_build =", is_win32_build

    cross = CrossPlatformBuilder(build_types,
                                 is_android_build,
                                 is_ios_build,
                                 is_win32_build)
    cross.build_all()


if __name__ == "__main__":
    main(sys.argv[1:])
