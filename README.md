# README #

This repository is based on the tutorial on http://mobilecpptutorials.com

---

I use the following toolchains:

* Android: https://github.com/cristeab/ios-cmake

* IOS: https://github.com/taka-no-me/android-cmake

---

Use "run_cmake_android_all.sh", "run_cmake_ios_all.sh" for a cmake project generation and build.

You could change and configure them as you want.

"run_cmake_win32_all.sh" - it's for Visual Studio console application.

"PROJECT_DIR/cmake" - it contains toolchains.